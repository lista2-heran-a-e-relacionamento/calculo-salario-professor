import java.util.Scanner;
import java.util.ArrayList;


public class Main
{
    static ViannaJr vj = new ViannaJr();
    
    public static void main(String[] args) {
        
        Professor p = cadastraProfessores();
        
        imprimeSalario();
        
    }

    public static void imprimeSalario() {
         
        Scanner le = new Scanner(System.in);
        
        System.out.println("Escolha o número correspondente do seu nome para saber seu salário");
        int num;
        for(int i = 0; i < vj.getProfessores().size(); i++) {
            System.out.println((i+1) + ")" + vj.getProfessores().get(i).getNome());
        }
        num = le.nextInt();
        
        System.out.println("O salário do " + vj.getProfessores().get(num -1).getNome() + " é de " + vj.getProfessores().get(num -1).totalSalario());
        
    }
    
    
    public static Disciplina cadastraDisciplinas() {
        Scanner le = new Scanner(System.in);
        
        Disciplina d = null;
        String resposta;
    
        while(true) {
            
            System.out.println("Informe se a disciplina a ser ministrada é EAD? (S)im/(N)ão");
             
            resposta = le.next();
            
            if(resposta.equalsIgnoreCase("N")){
                d = new Disciplina();
            } else if (resposta.equalsIgnoreCase("S")) {
                d = new DisciplinaEad();   
            }
            
            System.out.println("Informe o nome da disciplina: ");
            d.setNomeDisciplina(le.next());
            
            System.out.println("Informe a quantidade de crédito: ");
            d.setQtdCreditosSemanais(le.nextInt());
            
            System.out.println("Gostaria de incluir mais disciplinas? (S)im/(N)ão");
            if (le.next().equalsIgnoreCase("N")) {
                break;   
            }
        }
        
        return d;
    }
    
    
    public static Professor cadastraProfessores() {
        Scanner le = new Scanner(System.in); 
        
        Professor p;
        
        
        while(true) {
            
            p = new Professor();
            
            System.out.println("Informe o nome do professor: ");
            p.setNome(le.next());
            System.out.println("Selecione qual é sua formação:  ");
            p.setNivelFormacao(formacaoAcademica());
            System.out.println("##CADASTRO DAS DISCIPLINAS E CRÉDITOS DO PROFESSOR " + p.getNome().toUpperCase()+"##");
            p.addDisciplina(cadastraDisciplinas());
            vj.addProfessor(p);
            System.out.println("Gostaria de incluir mais professores? (S)im/(N)ão");
            
            if(le.next().equalsIgnoreCase("N")) {
                break;   
            }
        
        }
        
        return p;
    }
    
    public static String formacaoAcademica() {
        Scanner le = new Scanner(System.in);
        String escolha;
        
        System.out.println("#1) Graduado");
        System.out.println("#2) Especialista");
        System.out.println("#3) Mestre");
        System.out.println("#4) Doutor");
          
        escolha = le.next();
        
        if( escolha.equals("1")) {
            escolha = "Graduado";   
        }else if (escolha.equals("2")) {
            escolha = "Especialista";
        }else if (escolha.equals("3")) {
            escolha = "Mestre";
        }else if (escolha.equals("4")) {
            escolha = "Doutor";
        }
        
        return escolha;
    }
}
