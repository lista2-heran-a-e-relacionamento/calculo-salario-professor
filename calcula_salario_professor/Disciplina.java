

public class Disciplina
{
    private String nomeDisciplina;
    private int qtdCreditosSemanais;
    private double valorPagoHoraAula;
    
    public Disciplina() {
        this.valorPagoHoraAula = 25.0;
    }
    
    public Disciplina(String nomeDisciplina, int qtdCreditos) {
        this.nomeDisciplina = nomeDisciplina;
        this. qtdCreditosSemanais =  0;
        this.valorPagoHoraAula = 25.0;
    }
    
    public double calculaValorPagoCeditos() {
        return (getQtdCreditosSemanais() * getValorPagoHoraAula()) * 4.0;
    }
    
    public double ValorPagoEspecialista() {
        return  calculaValorPagoCeditos() * 1.15;   
    }
    
    public double ValorPagoMestre() {
        return calculaValorPagoCeditos() * 1.45;   
    }
    
    public double ValorPagoDoutor() {
        return  calculaValorPagoCeditos() * 1.75 ;   
    }
    
    public int getQtdCreditosSemanais() {
        return this. qtdCreditosSemanais;   
    }
    
    public void setQtdCreditosSemanais(int qtdCreditosSemanais) {
        this. qtdCreditosSemanais =  qtdCreditosSemanais;   
    }
    
    public String getNomeDisciplina() {
        return this.nomeDisciplina;     
    }
    
    public void setNomeDisciplina(String nomeDisciplina) {
        this.nomeDisciplina = nomeDisciplina;   
    }
    
    public double getValorPagoHoraAula() {
        return this.valorPagoHoraAula;   
    }
    
}
