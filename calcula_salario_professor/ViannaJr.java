import java.util.ArrayList;


public class ViannaJr
{
    private ArrayList<Professor> professores;
    
    public ViannaJr() {
        professores = new ArrayList<>();
    }
    
    public void addProfessor(Professor professor) {
        this.professores.add(professor);   
    }
    
    public void removeProfessor(Professor professor) {
        this.professores.remove(professor);   
    }
    
    public ArrayList<Professor> getProfessores() {
        return this.professores;   
    }
}
