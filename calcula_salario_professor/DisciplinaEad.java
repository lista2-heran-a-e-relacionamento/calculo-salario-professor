

public class DisciplinaEad extends Disciplina
{
    
    public DisciplinaEad() {}
    
    public DisciplinaEad(String nomeDisciplina, int qtdCreditos) {
        super(nomeDisciplina, qtdCreditos);
    }
    
    @Override
    public double calculaValorPagoCeditos() {
        return (getQtdCreditosSemanais() * 4) * (getValorPagoHoraAula() - (getValorPagoHoraAula()*0.25));
    }
    

}
