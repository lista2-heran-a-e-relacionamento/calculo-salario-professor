import java.util.ArrayList;


public class Professor
{
    private String nome, nivelFormacao;
    private ArrayList<Disciplina> disciplinas;
    
    public Professor() {
        disciplinas = new ArrayList<>();   
    }
    
    public Professor(String nome, String nivelFormacao) {
        this.nome = nome;
        this.nivelFormacao = nivelFormacao;
        disciplinas = new ArrayList<>();
    }
    
    public double totalSalario() {
        double total = 0;
        
        for (Disciplina d : disciplinas) {
            if(getNivelFormacao().equalsIgnoreCase("Graduado")) {
                total += d.calculaValorPagoCeditos();
            } else if (getNivelFormacao().equalsIgnoreCase("Especialista")) {
                total += d.ValorPagoEspecialista();   
            } else if (getNivelFormacao().equalsIgnoreCase("Mestre")) {
                total += d.ValorPagoMestre();   
            } else if (getNivelFormacao().equalsIgnoreCase("Doutor")) {
                total += d.ValorPagoDoutor();   
            }
        }
        
        return total;
    }
    
    public void addDisciplina(Disciplina disciplina) {
        this.disciplinas.add(disciplina);   
    }
    
    public void removeDisciplina(Disciplina disciplina) {
        this.disciplinas.remove(disciplina);   
    }
    
    public String getNivelFormacao() {
        return this.nivelFormacao;   
    }
    
    public void setNivelFormacao(String nivelFormacao) {
        this.nivelFormacao = nivelFormacao;   
    }
    
    public String getNome() {
        return this.nome;    
    }
    
    public void setNome(String nome) {
        this.nome = nome;   
    }
    
    public ArrayList<Disciplina> getDisciplina() {
        return this.disciplinas;   
    }
    
}
